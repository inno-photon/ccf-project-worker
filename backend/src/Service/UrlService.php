<?php

namespace App\Service;

use Psr\Log\LoggerInterface;

class UrlService
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Getting full url to parse.
     *
     * @param string $url - url that we are going to begin crawling with
     * @param string $href - value of href attribute in <a> tag
     * @return string
     */
    public function getFullUrl(string $url, string $href): string
    {
        $originalHref = $href;
        $parts = [];
        /**
         * If it is a relative address
         */
        if (0 !== strpos($href, 'http')) {
            $parts = parse_url($url);
            $path = $parts['path'];
            $href = $parts['scheme'] . '://';
            if (isset($parts['user']) && isset($parts['pass'])) {
                $href .= $parts['user'] . ':' . $parts['pass'] . '@';
            }
            $href .= $parts['host'];
            if (isset($parts['port'])) {
                $href .= ':' . $parts['port'];
            }
            if (0 !== strpos($originalHref, '/')) {
                if (strlen($path) - 1 !== strrpos($path, '/')) { //IF the $path ends with '/'
                    $path = dirname($path);
                }
                $path = trim($path, '/');

                if (!empty($path)) {
                    $path .= '/';
                }
                $href .= '/' . $path . ltrim($originalHref, '/');
            } else {
                $href .= '/' . ltrim($originalHref, '/');
            }
//            }
            $this->logger->debug('UrlService:relative_url_found', [
                'original_href' => $originalHref,
                'full_href'     => $href,
                'url'           => $url,
                'url_parts'     => $parts,
            ]);
        }

        return $href;
    }
}
